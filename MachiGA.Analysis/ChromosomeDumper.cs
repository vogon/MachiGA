﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MachiGA.Core;

namespace MachiGA.Analysis
{
    class ChromosomeDumper
    {
        private const string DATASET_DIR = @"D:\Projects\MachiGA\dataset_0";
        private const string OUTPUT_DIR = @"D:\Projects\MachiGA\dataset_0_derived";
        private static readonly Regex POPULATION_RE = new Regex(@"gen[0-9]+\.bin");
        private const int GAMES_PER_MATCH = 21;

        static void DumpCSV<T>(string path, IEnumerable<T> items, string[] headers, Func<T, string>[] extractors)
        {
            using (FileStream csv = File.Open(path, FileMode.Create))
            {
                StreamWriter w = new StreamWriter(csv);

                if (headers != null)
                {
                    w.WriteLine(string.Join(",", headers));
                }

                foreach (T item in items)
                {
                    string[] values = extractors.Select(e => e.Invoke(item)).ToArray();

                    w.WriteLine(string.Join(",", values));
                }

                w.Flush();
            }
        }

        static void DumpDecisionFunction2CSV(string path, IEnumerable<Chromosome2> chrs, 
            Func<Chromosome2, DecisionFunction2> dfExtractor, Func<Chromosome2, string> roundsExtractor)
        {
            List<string> headers = new List<string>();
            headers.Add("rounds");
            headers.Add("k");
            headers.Add("again");
            headers.Add("my$");
            headers.Add("opp$");

            List<Func<Chromosome2, string>> extractors = new List<Func<Chromosome2,string>>();
            extractors.Add(roundsExtractor);
            extractors.Add(chr => dfExtractor.Invoke(chr).K.ToString());
            extractors.Add(chr => dfExtractor.Invoke(chr).TakeAnotherTurn.ToString());
            extractors.Add(chr => dfExtractor.Invoke(chr).MyBankAccount.ToString());
            extractors.Add(chr => dfExtractor.Invoke(chr).OpponentBankAccount.ToString());

            foreach (Establishment e in Enum.GetValues(typeof(Establishment)))
            {
                headers.Add(string.Format("myE_{0}", (int)e));
                headers.Add(string.Format("oppE_{0}", (int)e));
                extractors.Add(chr => dfExtractor.Invoke(chr).MyEstablishments[e].ToString());
                extractors.Add(chr => dfExtractor.Invoke(chr).OpponentEstablishments[e].ToString());
            }

            foreach (Landmarks l in Extensions.AllLandmarks)
            {
                headers.Add(string.Format("myL_{0}", (int)l));
                headers.Add(string.Format("oppL_{0}", (int)l));
                extractors.Add(chr => dfExtractor.Invoke(chr).MyLandmarks[l].ToString());
                extractors.Add(chr => dfExtractor.Invoke(chr).OpponentLandmarks[l].ToString());
            }

            DumpCSV(path, chrs, headers.ToArray(), extractors.ToArray());
        }

        static void Main(string[] args)
        {
            List<Chromosome2> fullPop = new List<Chromosome2>();
            int popCount = 0;

            foreach (string file in Directory.EnumerateFiles(DATASET_DIR))
            {
                if (!POPULATION_RE.Match(file).Success) continue;

                using (FileStream dump = File.Open(file, FileMode.Open))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    
                    Chromosome2[] pop = (Chromosome2[])formatter.Deserialize(dump);

                    fullPop.AddRange(pop);
                    popCount += pop.Length;
                }

                Console.Clear();
                Console.WriteLine("{0} chromosomes loaded...", popCount);
            }

            // rank some dudes
            ITournament tourney = new SwissTournament(GAMES_PER_MATCH, fullPop.Select(chr => new Agent2(chr)).ToList());

            tourney.Start();
            tourney.AwaitCompletion();

            // strip the chromosomes out of each agent
            Dictionary<Chromosome2, double> chrRounds = new Dictionary<Chromosome2, double>();

            foreach (KeyValuePair<IAgent, double> kvp in tourney.Results)
            {
                Agent2 agent = (Agent2)kvp.Key;
                Chromosome2 chr = agent.Chromosome;

                chrRounds[chr] = kvp.Value;
            }

            // dump some data
            DumpDecisionFunction2CSV(OUTPUT_DIR + @"\rolltwodice.csv", fullPop,
                chr => chr.RollTwoDice, c => chrRounds[c].ToString());

            foreach (Establishment e in Enum.GetValues(typeof(Establishment)))
            {
                DumpDecisionFunction2CSV(string.Format(@"{0}\buyestablishment_{1}.csv", OUTPUT_DIR, e),
                    fullPop, chr => chr.BuyEstablishments[e], c => chrRounds[c].ToString());
            }

            foreach (Landmarks l in Extensions.AllLandmarks)
            {
                DumpDecisionFunction2CSV(string.Format(@"{0}\buylandmark_{1}.csv", OUTPUT_DIR, l),
                    fullPop, chr => chr.BuyLandmarks[l], c => chrRounds[c].ToString());
            }

            List<string> rerollHeaders = new List<string>();
            rerollHeaders.Add("rounds");

            List<Func<Chromosome2, string>> rerollExtractors = new List<Func<Chromosome2,string>>();
            rerollExtractors.Add(c => chrRounds[c].ToString());

            for (int i = 1; i <= 12; i++)
            {
                int k = i;

                rerollHeaders.Add(k.ToString());
                rerollExtractors.Add(c => c.Reroll[k] ? "1" : "0");
            }

            DumpCSV(OUTPUT_DIR + @"\reroll.csv", fullPop, rerollHeaders.ToArray(), rerollExtractors.ToArray());
        }
    }
}
