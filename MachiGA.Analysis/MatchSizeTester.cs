﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MachiGA.Core;

namespace MachiGA.Analysis
{
    class MatchSizeTester
    {
        private static Agent2[] population;
        private static int nMatches;
        private static int[] flips;

        private const int MAX_MATCH_LENGTH = 21;

        private static void DoMatch(int aId, int bId)
        {
            Agent2 a = population[aId], b = population[bId];
            int[] leader = new int[MAX_MATCH_LENGTH];
            int aGameWins = 0, bGameWins = 0;

            for (int game = 0; game < MAX_MATCH_LENGTH; game++)
            {
                // always let the current underdog go first to try and balance series
                // out if the first/second player has an advantage
                bool aIsWhite = (aGameWins < bGameWins);

                Game g = new Game(CardSets.MiniMachi,
                    new Agent2[] {
                                aIsWhite ? a : b,
                                aIsWhite ? b : a
                            });

                GameResult result = g.Play();

                if (result.Winner == 0)
                {
                    aGameWins += aIsWhite ? 1 : 0;
                    bGameWins += aIsWhite ? 0 : 1;
                }
                else
                {
                    aGameWins += aIsWhite ? 0 : 1;
                    bGameWins += aIsWhite ? 1 : 0;
                }

                if (aGameWins > bGameWins)
                {
                    // A leads
                    leader[game] = 0;
                }
                else if (bGameWins > aGameWins)
                {
                    // B leads
                    leader[game] = 1;
                }
                else
                {
                    // tied
                    leader[game] = -1;
                }

                if ((aGameWins > MAX_MATCH_LENGTH / 2) || (bGameWins > MAX_MATCH_LENGTH / 2))
                {
                    bool leadChanged = false;

                    // someone won the match
                    lock (flips)
                    {
                        for (int scanBack = game; scanBack >= 0; scanBack--)
                        {
                            if (leader[scanBack] != leader[game])
                            {
                                // lead has changed hands
                                leadChanged = true;
                            }

                            if (leadChanged)
                            {
                                flips[scanBack]++;
                            }
                        }

                        nMatches++;
                    }

                    break;
                }
            }
        }

        static void Main(string[] args)
        {
            const int POPULATION_SIZE = 100;
            Random rng = new Random();

            population = new Agent2[POPULATION_SIZE];
            flips = new int[MAX_MATCH_LENGTH];

            // generate initial population
            for (int i = 0; i < POPULATION_SIZE; i++)
            {
                population[i] = new Agent2(Chromosome2.Random(rng));
            }

            Thread worker = new Thread(
                    () => Parallel.For(0, population.Length - 1, a =>
                        Parallel.For(a + 1, population.Length, b =>
                            DoMatch(a, b)
                        )
                    )
                );

            worker.Start();

            while (!worker.Join(100))
            {
                int maxFlip = 0;

                for (int i = 0; i < MAX_MATCH_LENGTH; i++)
                {
                    if (flips[i] > 0) maxFlip = i;
                }

                Console.Clear();
                Console.WriteLine("{0} matches done (max {1})...", nMatches, maxFlip);
            }

            using (TextWriter w = new StreamWriter(new FileStream("D:\\projects\\machiga\\match_length.csv", FileMode.Create)))
            {
                w.WriteLine("0,{0}", nMatches);

                for (int i = 0; i < MAX_MATCH_LENGTH; i++)
                {
                    w.WriteLine("{0},{1}", i + 1, flips[i]);
                }
            }
        }
    }
}
