﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachiGA.Core
{
    [Flags]
    public enum CardSets
    {
        None = 0,
        MiniMachi = 1,  // a smaller version of the game for development purposes
        BaseGame = 2,
        GamingMegaStore = 4,
    }
}
