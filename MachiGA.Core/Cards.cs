﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachiGA.Core
{
    public enum Establishment
    {
        WheatField,
        Ranch,
        Forest,
        Mine,
        AppleOrchard,
        Bakery,
        ConvenienceStore,
        CheeseFactory,
        FurnitureFactory,
        FruitAndVegMarket,
        Cafe,
        FamilyRestaurant,
        Stadium,
        BusinessCenter,
        TVStation,
        GamingMegaStore
    }

    [Flags]
    public enum Landmarks
    {
        None = 0,
        AmusementPark = 1,
        RadioTower = 2,
        ShoppingMall = 4,
        TrainStation = 8,
        All = AmusementPark | RadioTower | ShoppingMall | TrainStation
    }
}
