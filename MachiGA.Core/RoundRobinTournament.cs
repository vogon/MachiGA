﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MachiGA.Core
{
    public class RoundRobinTournament : ITournament
    {
        private int gamesPerMatch;
        private TournamentStatistics stats;
        private List<CompetitorStatistics> competitors;

        private Thread tournamentThread;

        private class CompetitorStatistics
        {
            public Agent2 Agent;
            public double Fitness;

            public CompetitorStatistics(Agent2 agent)
            {
                this.Agent = agent;
                this.Fitness = 0;
            }
        }

        public RoundRobinTournament(int gamesPerMatch, IEnumerable<Agent2> competitors)
        {
            this.gamesPerMatch = gamesPerMatch;

            this.competitors = competitors.Select(c => new CompetitorStatistics(c)).ToList();
            this.stats = new TournamentStatistics();
        }

        private static double GameFitness(GameResult result, int player)
        {
            if (player == result.Winner)
            {
                return Math.Max(0, (1 - (0.001 * result.NTurns)) * ((result.Ending == GameEnding.Tko) ? 0.75 : 1));
            }
            else
            {
                return 0;
            }
        }

        private void DoMatch(int aId, int bId)
        {
            Match m = new Match(gamesPerMatch, this.competitors[aId].Agent, this.competitors[bId].Agent, GameFitness);

            m.Play();

            MatchStatistics matchStats = m.Statistics;

            stats.ReportGame(matchStats.Turns);

            if (matchStats.Winner == 0)
            {
                this.competitors[aId].Fitness += matchStats.WinnerFitness;
            }
            else
            {
                this.competitors[bId].Fitness += matchStats.WinnerFitness;
            }

            stats.ReportMatch(matchStats.Games, matchStats.Tkos);
        }

        public void Start()
        {
            this.tournamentThread = new Thread(
                () => Parallel.For(0, this.competitors.Count - 1, a => 
                    Parallel.For(a + 1, this.competitors.Count, b =>
                        DoMatch(a, b)
                    )
                )
            );

            this.tournamentThread.Start();
        }

        public void AwaitCompletion()
        {
            while (!this.tournamentThread.Join(0))
            {
                Console.Write("\rtournament running; {0} matches and {1} games done.", stats.Matches, stats.Games);
                Thread.Sleep(100);
            }
        }

        public TournamentStatistics Statistics
        {
            get
            {
                return this.stats;
            }
        }

        public Dictionary<IAgent, double> Results
        {
            get
            {
                return this.competitors.ToDictionary(c => (IAgent)c.Agent, c => c.Fitness);
            }
        }
    }
}
