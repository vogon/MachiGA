﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachiGA.Core
{
    [Serializable]
    public class TournamentStatistics
    {
        public int Tkos, Games, Matches, Turns;

        public TournamentStatistics()
        {
            Tkos = Games = Matches = Turns = 0;
        }

        public void ReportGame(int turns)
        {
            lock (this)
            {
                this.Turns += turns;
            }
        }

        public void ReportMatch(int games, int tkos)
        {
            lock (this)
            {
                this.Matches++;
                this.Games += games;
                this.Tkos += tkos;
            }
        }
    }
}
