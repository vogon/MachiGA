﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MachiGA.Core
{
    public enum GameEnding
    {
        Finished,
        Tko
    }

    public struct GameResult
    {
        public int NTurns;
        public GameEnding Ending;
        public int Winner;
    }

    public class Game
    {
        private IAgent[] agents;
        private GameState state;

        public Game(CardSets cardSets, IAgent[] agents)
        {
            this.agents = agents;
            this.state = GameState.Initial(agents.Length, cardSets);
        }

        private void RollDice(int n, out int die1, out int? die2)
        {
            Random rng = new Random();

            die1 = rng.Next(6) + 1;

            if (n == 2)
            {
                die2 = rng.Next(6) + 1;
            }
            else
            {
                die2 = null;
            }
        }

        private void DoEveryTurnPayout(Establishment e, int payout)
        {
            for (int i = 0; i < state.Players.Length; i++)
            {
                int count = 0;
                state.Players[i].Establishments.TryGetValue(e, out count);

                state.Players[i].BankAccount += payout * count;
            }
        }

        private void DoActiveTurnPayout(Establishment e, int payout)
        {
            int count = 0;
            state.Players[state.ActivePlayer].Establishments.TryGetValue(e, out count);

            state.Players[state.ActivePlayer].BankAccount += payout * count;
        }

        private void DoRestaurantPayout(Establishment e, int payout)
        {
            List<int> playerOrder = new List<int>();

            // payouts occur in reverse player order, starting at the active player's left
            for (int i = state.ActivePlayer - 1; i >= 0; i--) playerOrder.Add(i);
            for (int i = state.Players.Length - 1; i > state.ActivePlayer; i--) playerOrder.Add(i);

            foreach (int to in playerOrder)
            {
                // count up the number of establishments to pay out to
                int count = 0;
                state.Players[to].Establishments.TryGetValue(e, out count);

                // compute the payout
                int totalPayoutThisPlayer = Math.Min(state.Players[state.ActivePlayer].BankAccount,
                    count * payout);

                // pay it out
                state.Players[state.ActivePlayer].BankAccount -= totalPayoutThisPlayer;
                state.Players[to].BankAccount += totalPayoutThisPlayer;
            }
        }

        private void DoActiveTurnFactory(Establishment e, int multiplier, Func<Establishment, bool> f)
        {
            int factoryCount = 0;
            state.Players[state.ActivePlayer].Establishments.TryGetValue(e, out factoryCount);

            int materialsCount = 0;

            foreach (Establishment materialEstab in Enum.GetValues(typeof(Establishment)))
            {
                if (f(materialEstab))
                {
                    int n = 0;
                    state.Players[state.ActivePlayer].Establishments.TryGetValue(e, out n);

                    materialsCount += n;
                }
            }

            state.Players[state.ActivePlayer].BankAccount +=
                factoryCount * multiplier * materialsCount;
        }

        public void TakeNextTurn()
        {
            // take the next player's turn. 
            int activePlayer = state.ActivePlayer;
            IAgent activeAgent = agents[activePlayer];

            // the turn has three steps:

            // =============== 1. roll dice ===============
            int nDice;

            if ((state.Players[activePlayer].Landmarks & Landmarks.TrainStation) != 0)
            {
                // if the player has a train station, check with agent if they want
                // to roll 1 or 2 dice.
                nDice = activeAgent.Roll(state, activePlayer);
            }
            else
            {
                nDice = 1;
            }

            int die1;
            int? die2;

            // roll.
            RollDice(nDice, out die1, out die2);
            //Console.WriteLine("player {0} rolls {1} dice ({2} + {3}).", activePlayer, nDice, die1, die2);

            // if this player has a radio tower, check with agent if they
            // want to reroll.
            if ((state.Players[activePlayer].Landmarks & Landmarks.RadioTower) != 0)
            {
                if (activeAgent.Reroll(state, die1, die2, activePlayer))
                {
                    //Console.WriteLine("player {0} rerolls ({1} + {2}).", activePlayer, die1, die2);
                    RollDice(nDice, out die1, out die2);
                }
            }

            // if the player rolled doubles and has an amusement park, give them another turn.
            if ((state.Players[activePlayer].Landmarks & Landmarks.AmusementPark) != 0 &&
                (die2.HasValue && (die1 == die2.Value)))
            {
                state.TakeAnotherTurn = true;
            }

            // 2. earn income
            // sum dice.
            int sum = die1 + (die2 ?? 0);

            // apply card effects.

            // the Shopping Mall landmark modifies some cards...
            bool hasShoppingMall = 
                (state.Players[activePlayer].Landmarks & Landmarks.ShoppingMall) != 0;

            switch (sum)
            {
                case 1:
                    DoEveryTurnPayout(Establishment.WheatField, 1);
                    break;
                case 2:
                    DoEveryTurnPayout(Establishment.Ranch, 1);
                    DoActiveTurnPayout(Establishment.Bakery, hasShoppingMall ? 2 : 1);
                    break;
                case 3:
                    DoRestaurantPayout(Establishment.Cafe, hasShoppingMall ? 2 : 1);
                    DoActiveTurnPayout(Establishment.Bakery, hasShoppingMall ? 2 : 1);
                    break;
                case 4:
                    DoActiveTurnPayout(Establishment.ConvenienceStore, hasShoppingMall ? 4 : 3);
                    break;
                case 5:
                    DoEveryTurnPayout(Establishment.Forest, 1);
                    break;
                case 7:
                    DoActiveTurnFactory(Establishment.CheeseFactory, 3, Extensions.IsCow);
                    break;
                case 8:
                    DoActiveTurnFactory(Establishment.FurnitureFactory, 3, Extensions.IsGear);
                    break;
                case 9:
                    DoRestaurantPayout(Establishment.FamilyRestaurant, hasShoppingMall ? 3 : 2);
                    DoEveryTurnPayout(Establishment.Mine, 5);
                    break;
                case 10:
                    DoRestaurantPayout(Establishment.FamilyRestaurant, hasShoppingMall ? 3 : 2);
                    DoEveryTurnPayout(Establishment.AppleOrchard, 3);
                    break;
                case 11:
                case 12:
                    DoActiveTurnFactory(Establishment.FruitAndVegMarket, 2, Extensions.IsWheat);
                    break;
            }

            // 3. construction
            // ask agent what they want to buy.
            BuyDecision decision = activeAgent.Buy(state, activePlayer);

            if (decision.Establishment != null)
            {
                // player wants to buy an establishment
                Establishment e = decision.Establishment.Value;

                // take the establishment off the market
                state.Market[e]--;

                // player pays for the establishment
                state.Players[activePlayer].BankAccount -= e.Cost();

                // give the player the establishment
                int count = 0;
                state.Players[activePlayer].Establishments.TryGetValue(e, out count);
                state.Players[activePlayer].Establishments[e] = count + 1;

                //Console.WriteLine("player {0} buys establishment {1} (${2}) and has ${3}.", activePlayer,
                //    decision.Establishment, decision.Establishment.Value.Cost(),
                //    state.Players[activePlayer].BankAccount);
            }
            else if (decision.Landmark != null)
            {
                // player wants to buy a landmark
                Landmarks l = decision.Landmark.Value;

                // player pays for the landmark
                state.Players[activePlayer].BankAccount -= l.Cost();

                // give the player the landmark
                state.Players[activePlayer].Landmarks |= l;

                //Console.WriteLine("player {0} buys landmark {1} (${2}) and has ${3}.", activePlayer,
                //    decision.Landmark, decision.Landmark.Value.Cost(),
                //    state.Players[activePlayer].BankAccount);
            }
            else
            {
                // player wants to pass
                //Console.WriteLine("player {0} passes and has ${1}.", activePlayer,
                //    state.Players[activePlayer].BankAccount);
            }

            // play proceeds to the next player in order, unless the 
            // player earned another turn through e.g. Amusement Park.
            if (state.TakeAnotherTurn)
            {
                state.TakeAnotherTurn = false;
            }
            else
            {
                state.ActivePlayer = (state.ActivePlayer + 1) % state.Players.Length;
            }
        }

        public GameResult Play()
        {
            GameResult result = new GameResult { NTurns = 0 };

            while (true)
            {
                TakeNextTurn();
                result.NTurns++;

                if (state.Winner().HasValue)
                {
                    result.Ending = GameEnding.Finished;
                    result.Winner = state.Winner().Value;
                    break;
                }

                if (state.Tko().HasValue)
                {
                    result.Ending = GameEnding.Tko;
                    result.Winner = state.Tko().Value;
                    break;
                }
            }

            return result;
        }
    }
}
