﻿using System;
using System.Collections.Generic;

namespace MachiGA.Core
{
    public interface ITournament
    {
        void Start();
        void AwaitCompletion();

        Dictionary<IAgent, double> Results { get; }
        TournamentStatistics Statistics { get; }
    }
}
