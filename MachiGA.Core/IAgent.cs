﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachiGA.Core
{
    public struct BuyDecision
    {
        public Establishment? Establishment;
        public Landmarks? Landmark;
    }

    public interface IAgent
    {
        // roll dice decisions
        int Roll(GameState state, int playerId);
        bool Reroll(GameState state, int die1, int? die2, int playerId);

        // earn income decisions
        int TargetPlayer(GameState state, Establishment e, int playerId);

        // construction decisions
        BuyDecision Buy(GameState state, int playerId);
    }
}
