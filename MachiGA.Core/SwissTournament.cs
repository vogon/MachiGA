﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MachiGA.Core
{
    public class SwissTournament : ITournament
    {
        private readonly int gamesPerMatch;
        private CompetitorStatistics[] competitors;
        private TournamentStatistics stats;
        private Thread tournamentThread;

        private int currentRound;
        private int pairsCompleted;

        private class CompetitorStatistics
        {
            public Agent2 Agent;

            public int RoundsWon;
            public int WinLossDifferential;

            public CompetitorStatistics(Agent2 agent)
            {
                this.Agent = agent;
                this.RoundsWon = 0;
                this.WinLossDifferential = 0;
            }
        }

        public SwissTournament(int gamesPerMatch, IList<Agent2> competitors)
        {
            this.gamesPerMatch = gamesPerMatch;
            this.competitors = competitors.Select(a => new CompetitorStatistics(a)).ToArray();
            this.stats = new TournamentStatistics();
        }

        private void DoMatch(int aId, int? bIdOrNull)
        {
            if (!bIdOrNull.HasValue)
            {
                // match is a bye; A automatically wins
                this.competitors[aId].RoundsWon++;
                return;
            }

            int bId = bIdOrNull.Value;
            Match m = new Match(gamesPerMatch, this.competitors[aId].Agent, this.competitors[bId].Agent,
                (result, player) => (result.Winner == player) ? 1 : -1);

            m.Play();

            MatchStatistics matchStats = m.Statistics;

            stats.ReportGame(matchStats.Turns);

            if (matchStats.Winner == 0)
            {
                this.competitors[aId].RoundsWon++;
                this.competitors[aId].WinLossDifferential += (int)matchStats.WinnerFitness;
                this.competitors[bId].WinLossDifferential -= (int)matchStats.WinnerFitness;
            }
            else
            {
                this.competitors[bId].RoundsWon++;
                this.competitors[aId].WinLossDifferential -= (int)matchStats.WinnerFitness;
                this.competitors[bId].WinLossDifferential += (int)matchStats.WinnerFitness;
            }

            stats.ReportMatch(matchStats.Games, matchStats.Tkos);
        }

        private void DoRound()
        {
            Parallel.For(0, (int)Math.Ceiling((double)this.competitors.Length / 2.0), i => {
                DoMatch(i * 2, i * 2 + 1);
                pairsCompleted++;
            });

            // sort competitors by rounds won (descending), ties broken by win-loss differential (descending)
            this.competitors = this.competitors.
                OrderByDescending(stats => stats.WinLossDifferential).
                OrderByDescending(stats => stats.RoundsWon).
                ToArray();
        }

        private void DoTournament()
        {
            int nRounds = (int)Math.Ceiling(Math.Log(competitors.Length) / Math.Log(2));

            for (int round = 0; round < nRounds; round++)
            {
                currentRound = round;
                pairsCompleted = 0;
                DoRound();
            }
        }

        void ITournament.Start()
        {
            this.tournamentThread = new Thread(DoTournament);

            this.tournamentThread.Start();
        }

        void ITournament.AwaitCompletion()
        {
            while (!this.tournamentThread.Join(0))
            {
                Console.Write("\rtournament running; round {0}, {1} pairs completed.", currentRound, pairsCompleted);
                Thread.Sleep(100);
            }
        }

        Dictionary<IAgent, double> ITournament.Results
        {
            get
            {
                return this.competitors.ToDictionary(cs => (IAgent)cs.Agent, cs => (double)cs.RoundsWon);
            }
        }

        TournamentStatistics ITournament.Statistics
        {
            get { return this.stats; }
        }
    }
}
