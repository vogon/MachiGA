﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachiGA.Core
{
    [Serializable]
    public struct DecisionFunction2
    {
        public double K;
        public double TakeAnotherTurn;
        public double MyBankAccount;
        public double OpponentBankAccount;
        public Dictionary<Establishment, double> MyEstablishments;
        public Dictionary<Establishment, double> OpponentEstablishments;
        public Dictionary<Landmarks, double> MyLandmarks;
        public Dictionary<Landmarks, double> OpponentLandmarks;

        public static DecisionFunction2 Random(Random rng = null, double bias = 0)
        {
            if (rng == null) rng = new Random();

            DecisionFunction2 fn = new DecisionFunction2
            {
                K = (rng.NextDouble() - 0.5 + bias) * 10,
                TakeAnotherTurn = (rng.NextDouble() - 0.5 + bias),
                MyBankAccount = (rng.NextDouble() - 0.5 + bias),
                OpponentBankAccount = (rng.NextDouble() - 0.5 + bias),
                MyEstablishments = new Dictionary<Establishment, double>(),
                OpponentEstablishments = new Dictionary<Establishment, double>(),
                MyLandmarks = new Dictionary<Landmarks, double>(),
                OpponentLandmarks = new Dictionary<Landmarks, double>()
            };

            foreach (Establishment e in Enum.GetValues(typeof(Establishment)))
            {
                fn.MyEstablishments[e] = rng.NextDouble() - 0.5 + bias;
                fn.OpponentEstablishments[e] = rng.NextDouble() - 0.5 + bias;
            }

            foreach (Landmarks l in Extensions.AllLandmarks)
            {
                fn.MyLandmarks[l] = rng.NextDouble() - 0.5 + bias;
                fn.OpponentLandmarks[l] = rng.NextDouble() - 0.5 + bias;
            }

            return fn;
        }

        /// <summary>
        ///     Mutate one double-valued component of a chromosome, with probability p,
        ///     by multiplying it by a random coefficient normally-distributed around 1.
        /// </summary>
        /// <param name="x">the original value</param>
        /// <param name="p">the probability of mutation</param>
        /// <param name="sigma">the SD of the random distribution</param>
        /// <param name="rng">the random number generator to use</param>
        /// <returns></returns>
        private double Mutate(double x, double p, double sigma, Random rng)
        {
            if (rng.NextDouble() < p)
            {
                return x * rng.BoxMuller(1, sigma);
            }
            else
            {
                return x;
            }
        }

        public DecisionFunction2 Mutate(double p, double sigma, Random rng = null)
        {
            if (rng == null) rng = new Random();

            DecisionFunction2 fn = new DecisionFunction2
            {
                K = Mutate(this.K, p, sigma, rng),
                TakeAnotherTurn = Mutate(this.TakeAnotherTurn, p, sigma, rng),
                MyBankAccount = Mutate(this.MyBankAccount, p, sigma, rng),
                OpponentBankAccount = Mutate(this.OpponentBankAccount, p, sigma, rng),
                MyEstablishments = new Dictionary<Establishment, double>(),
                OpponentEstablishments = new Dictionary<Establishment, double>(),
                MyLandmarks = new Dictionary<Landmarks, double>(),
                OpponentLandmarks = new Dictionary<Landmarks, double>()
            };

            foreach (Establishment e in MyEstablishments.Keys)
            {
                fn.MyEstablishments[e] = Mutate(MyEstablishments[e], p, sigma, rng);
            }

            foreach (Establishment e in OpponentEstablishments.Keys)
            {
                fn.OpponentEstablishments[e] = Mutate(OpponentEstablishments[e], p, sigma, rng);
            }

            foreach (Landmarks l in MyLandmarks.Keys)
            {
                fn.MyLandmarks[l] = Mutate(MyLandmarks[l], p, sigma, rng);
            }

            foreach (Landmarks l in OpponentLandmarks.Keys)
            {
                fn.OpponentLandmarks[l] = Mutate(OpponentLandmarks[l], p, sigma, rng);
            }

            return fn;
        }

        public static DecisionFunction2[] Crossover(DecisionFunction2 mother, DecisionFunction2 father, Random rng = null)
        {
            if (rng == null) rng = new Random();

            var children = new DecisionFunction2[2];

            children[0] = new DecisionFunction2()
            {
                MyEstablishments = new Dictionary<Establishment, double>(),
                OpponentEstablishments = new Dictionary<Establishment, double>(),
                MyLandmarks = new Dictionary<Landmarks, double>(),
                OpponentLandmarks = new Dictionary<Landmarks, double>()
            };
            children[1] = new DecisionFunction2()
            {
                MyEstablishments = new Dictionary<Establishment, double>(),
                OpponentEstablishments = new Dictionary<Establishment, double>(),
                MyLandmarks = new Dictionary<Landmarks, double>(),
                OpponentLandmarks = new Dictionary<Landmarks, double>()
            };

            Extensions.Crossover(mother.K, father.K, rng, out children[0].K, out children[1].K);
            Extensions.Crossover(mother.TakeAnotherTurn, father.TakeAnotherTurn, rng,
                out children[0].TakeAnotherTurn, out children[1].TakeAnotherTurn);
            Extensions.Crossover(mother.MyBankAccount, father.MyBankAccount, rng, 
                out children[0].MyBankAccount, out children[1].MyBankAccount);
            Extensions.Crossover(mother.OpponentBankAccount, father.OpponentBankAccount, rng,
                out children[0].OpponentBankAccount, out children[1].OpponentBankAccount);

            foreach (Establishment e in mother.MyEstablishments.Keys)
            {
                double child0, child1;

                Extensions.Crossover(mother.MyEstablishments[e], father.MyEstablishments[e], rng,
                    out child0, out child1);

                children[0].MyEstablishments[e] = child0;
                children[1].MyEstablishments[e] = child1;
            }

            foreach (Establishment e in mother.OpponentEstablishments.Keys)
            {
                double child0, child1;

                Extensions.Crossover(mother.OpponentEstablishments[e], father.OpponentEstablishments[e], rng,
                    out child0, out child1);

                children[0].OpponentEstablishments[e] = child0;
                children[1].OpponentEstablishments[e] = child1;
            }

            foreach (Landmarks l in mother.MyLandmarks.Keys)
            {
                double child0, child1;

                Extensions.Crossover(mother.MyLandmarks[l], father.MyLandmarks[l], rng,
                    out child0, out child1);

                children[0].MyLandmarks[l] = child0;
                children[1].MyLandmarks[l] = child1;
            }

            foreach (Landmarks l in mother.OpponentLandmarks.Keys)
            {
                double child0, child1;

                Extensions.Crossover(mother.OpponentLandmarks[l], father.OpponentLandmarks[l], rng,
                    out child0, out child1);

                children[0].OpponentLandmarks[l] = child0;
                children[1].OpponentLandmarks[l] = child1;
            }

            return children;
        }

        public double Evaluate(GameState state, int me)
        {
            int opponent = (me == 0) ? 1 : 0;
            double result = K;

            if (state.TakeAnotherTurn) result += TakeAnotherTurn;

            result += state.Players[me].BankAccount * MyBankAccount;
            result += state.Players[opponent].BankAccount * OpponentBankAccount;

            foreach (Establishment e in MyEstablishments.Keys)
            {
                int count = 0;
                state.Players[me].Establishments.TryGetValue(e, out count);

                result += count * MyEstablishments[e];
            }

            foreach (Establishment e in OpponentEstablishments.Keys)
            {
                int count = 0;
                state.Players[opponent].Establishments.TryGetValue(e, out count);

                result += count * OpponentEstablishments[e];
            }

            foreach (Landmarks l in MyLandmarks.Keys)
            {
                if ((state.Players[me].Landmarks & l) != 0)
                {
                    result += MyLandmarks[l];
                }
            }

            foreach (Landmarks l in OpponentLandmarks.Keys)
            {
                if ((state.Players[opponent].Landmarks & l) != 0)
                {
                    result += OpponentLandmarks[l];
                }
            }

            return result;
        }
    }

    [Serializable]
    public struct Chromosome2
    {
        public DecisionFunction2 RollTwoDice;
        public Dictionary<Establishment, DecisionFunction2> BuyEstablishments;
        public Dictionary<Landmarks, DecisionFunction2> BuyLandmarks;
        public Dictionary<int, bool> Reroll;

        public static Chromosome2 Random(Random rng = null)
        {
            if (rng == null) rng = new Random();

            Chromosome2 chr = new Chromosome2
            {
                RollTwoDice = DecisionFunction2.Random(rng),
                BuyEstablishments = new Dictionary<Establishment, DecisionFunction2>(),
                BuyLandmarks = new Dictionary<Landmarks, DecisionFunction2>(),
                Reroll = new Dictionary<int, bool>()
            };

            foreach (Establishment e in Enum.GetValues(typeof(Establishment)))
            {
                chr.BuyEstablishments[e] = DecisionFunction2.Random(rng);
            }

            foreach (Landmarks l in Extensions.AllLandmarks)
            {
                chr.BuyLandmarks[l] = DecisionFunction2.Random(rng, 0.5);
            }

            for (int i = 1; i <= 12; i++)
            {
                chr.Reroll[i] = (rng.NextDouble() <= 0.5);
            }

            return chr;
        }

        public Chromosome2 Mutate(double p, double sigma, Random rng = null)
        {
            if (rng == null) rng = new Random();

            Chromosome2 chr = new Chromosome2
            {
                RollTwoDice = this.RollTwoDice.Mutate(p, sigma, rng),
                BuyEstablishments = new Dictionary<Establishment, DecisionFunction2>(),
                BuyLandmarks = new Dictionary<Landmarks, DecisionFunction2>(),
                Reroll = new Dictionary<int, bool>()
            };

            foreach (Establishment e in Enum.GetValues(typeof(Establishment)))
            {
                chr.BuyEstablishments[e] = this.BuyEstablishments[e].Mutate(p, sigma, rng);
            }

            foreach (Landmarks l in Extensions.AllLandmarks)
            {
                chr.BuyLandmarks[l] = this.BuyLandmarks[l].Mutate(p, sigma, rng);
            }

            foreach (int i in this.Reroll.Keys)
            {
                chr.Reroll[i] = (rng.NextDouble() < p) ? !this.Reroll[i] : this.Reroll[i];
            }

            return chr;
        }

        public static Chromosome2[] Crossover(Chromosome2 mother, Chromosome2 father, Random rng = null)
        {
            if (rng == null) rng = new Random();

            var children = new Chromosome2[2];

            children[0] = new Chromosome2()
            {
                BuyEstablishments = new Dictionary<Establishment, DecisionFunction2>(),
                BuyLandmarks = new Dictionary<Landmarks, DecisionFunction2>(),
                Reroll = new Dictionary<int, bool>()
            };
            children[1] = new Chromosome2()
            {
                BuyEstablishments = new Dictionary<Establishment, DecisionFunction2>(),
                BuyLandmarks = new Dictionary<Landmarks, DecisionFunction2>(),
                Reroll = new Dictionary<int, bool>()
            };

            Extensions.Crossover(mother.RollTwoDice, father.RollTwoDice, rng, 
                out children[0].RollTwoDice, out children[1].RollTwoDice);

            foreach (Establishment e in mother.BuyEstablishments.Keys)
            {
                DecisionFunction2[] childFunctions =
                    DecisionFunction2.Crossover(mother.BuyEstablishments[e], father.BuyEstablishments[e], rng);

                children[0].BuyEstablishments[e] = childFunctions[0];
                children[1].BuyEstablishments[e] = childFunctions[1];

                //DecisionFunction2 child0, child1;

                //Extensions.Crossover(mother.BuyEstablishments[e], father.BuyEstablishments[e], rng, out child0, out child1);
                //children[0].BuyEstablishments[e] = child0;
                //children[1].BuyEstablishments[e] = child1;
            }

            foreach (Landmarks l in mother.BuyLandmarks.Keys)
            {
                DecisionFunction2[] childFunctions =
                    DecisionFunction2.Crossover(mother.BuyLandmarks[l], father.BuyLandmarks[l], rng);

                children[0].BuyLandmarks[l] = childFunctions[0];
                children[1].BuyLandmarks[l] = childFunctions[1];

                //DecisionFunction2 child0, child1;

                //Extensions.Crossover(mother.BuyLandmarks[l], father.BuyLandmarks[l], rng, out child0, out child1);
                //children[0].BuyLandmarks[l] = child0;
                //children[1].BuyLandmarks[l] = child1;
            }

            foreach (int i in mother.Reroll.Keys)
            {
                bool child0, child1;

                Extensions.Crossover(mother.Reroll[i], father.Reroll[i], rng, out child0, out child1);

                children[0].Reroll[i] = child0;
                children[1].Reroll[i] = child1;
            }
            
            return children;
        }
    }

    public class Agent2 : IAgent
    {
        public Chromosome2 Chromosome { get; private set; }

        public Agent2(Chromosome2 chr)
        {
            this.Chromosome = chr;
        }

        public int Roll(GameState state, int playerId)
        {
            return (Chromosome.RollTwoDice.Evaluate(state, playerId) > 0) ? 2 : 1;
        }

        public bool Reroll(GameState state, int die1, int? die2, int playerId)
        {
            int sum = die1 + (die2 ?? 0);

            return Chromosome.Reroll[sum];
        }

        public int TargetPlayer(GameState state, Establishment e, int playerId)
        {
            // in a 2-player game, always target the (single) opponent
            return (playerId == 0) ? 1 : 0;
        }

        public BuyDecision Buy(GameState state, int playerId)
        {
            double maxValue = 0;
            BuyDecision decision = new BuyDecision();

            foreach (Establishment e in Chromosome.BuyEstablishments.Keys)
            {
                //Console.Write("buy: evaluating {0}... ", e);

                // skip illegal moves
                if (!state.CanBuy(playerId, e))
                {
                    //Console.WriteLine("skipped.");
                    continue;
                }

                double value = Chromosome.BuyEstablishments[e].Evaluate(state, playerId);
                //Console.WriteLine(value);

                if (value > maxValue)
                {
                    decision.Landmark = null;
                    decision.Establishment = e;
                    maxValue = value;
                }
            }

            foreach (Landmarks l in Chromosome.BuyLandmarks.Keys)
            {
                //Console.Write("buy: evaluating {0}... ", l);

                // skip illegal moves
                if (!state.CanBuy(playerId, l))
                {
                    //Console.WriteLine("skipped.");
                    continue;
                }

                double value = Chromosome.BuyLandmarks[l].Evaluate(state, playerId);
                //Console.WriteLine(value);

                if (value > maxValue)
                {
                    decision.Landmark = l;
                    decision.Establishment = null;
                    maxValue = value;
                }
            }

            return decision;
        }
    }
}
