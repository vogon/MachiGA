﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachiGA.Core
{
    public struct GameState
    {
        public int ActivePlayer;
        public bool TakeAnotherTurn;

        public Dictionary<Establishment, int> Market;
        public PlayerState[] Players;

        public static GameState Initial(int nPlayers, CardSets cardSets)
        {
            GameState state = new GameState
            {
                ActivePlayer = 0,
                TakeAnotherTurn = false,
                Market = new Dictionary<Establishment, int>(),
                Players = new PlayerState[nPlayers]
            };

            // populate initial establishment stock
            foreach (Establishment e in Enum.GetValues(typeof(Establishment)))
            {
                state.Market[e] = e.InitialStock(cardSets);
            }

            // populate player state
            for (int i = 0; i < nPlayers; i++)
            {
                state.Players[i] = PlayerState.Initial(cardSets);
            }

            return state;
        }

        public bool CanBuy(int buyer, Establishment e)
        {
            // can't buy if sold out
            if (Market[e] == 0) return false;

            // can't buy if too poor
            if (Players[buyer].BankAccount < e.Cost()) return false;

            // can't buy multiples of a major establishment
            int count = 0;
            Players[buyer].Establishments.TryGetValue(e, out count);

            if (e.IsMajor() && count > 0) return false;

            // otherwise OK
            return true;
        }

        public bool CanBuy(int buyer, Landmarks l)
        {
            // can't buy if too poor
            if (Players[buyer].BankAccount < l.Cost()) return false;

            // can't buy if already owned
            if ((Players[buyer].Landmarks & l) != 0) return false;

            // otherwise OK
            return true;
        }

        public int? Winner()
        {
            for (int i = 0; i < Players.Length; i++)
            {
                if (Players[i].IsWinner()) return i;
            }

            return null;
        }

        public int? Tko()
        {
            // TODO: more rigorous metric
            int leadingLandmarkNetWorth = 0, secondLandmarkNetWorth = 0;
            int? leadingLandmarkPlayer = null, secondLandmarkPlayer = null;

            for (int i = 0; i < Players.Length; i++)
            {
                int landmarkNetWorth = Players[i].BankAccount + Players[i].Landmarks.Cost();

                if (landmarkNetWorth > leadingLandmarkNetWorth)
                {
                    secondLandmarkNetWorth = leadingLandmarkNetWorth;
                    secondLandmarkPlayer = leadingLandmarkPlayer;

                    leadingLandmarkNetWorth = landmarkNetWorth;
                    leadingLandmarkPlayer = i;
                }
                else if (landmarkNetWorth > secondLandmarkNetWorth)
                {
                    secondLandmarkNetWorth = landmarkNetWorth;
                    secondLandmarkPlayer = i;
                }
            }

            if (leadingLandmarkNetWorth > 52 && leadingLandmarkNetWorth - secondLandmarkNetWorth > 26)
            {
                return leadingLandmarkPlayer;
            }

            return null;
        }
    }

    public struct PlayerState
    {
        public Landmarks Landmarks;
        public Dictionary<Establishment, int> Establishments;
        public int BankAccount;

        public static PlayerState Initial(CardSets cardSets)
        {
            PlayerState state = new PlayerState
            {
                Landmarks = Landmarks.None,
                Establishments = new Dictionary<Establishment, int>(),
                BankAccount = 3
            };

            state.Establishments[Establishment.WheatField] = 1;
            state.Establishments[Establishment.Bakery] = 1;

            return state;
        }

        public bool IsWinner()
        {
            return (this.Landmarks == Landmarks.All);
        }
    }
}
