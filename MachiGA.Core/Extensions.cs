﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachiGA.Core
{
    public static class Extensions
    {
        public static Landmarks[] AllLandmarks =
            new Landmarks[] { Landmarks.AmusementPark, Landmarks.RadioTower, 
                Landmarks.ShoppingMall, Landmarks.TrainStation };

        public static int InitialStock(this Establishment e, CardSets cardSets)
        {
            switch (e)
            {
                case Establishment.AppleOrchard:
                case Establishment.Bakery:
                case Establishment.Cafe:
                case Establishment.CheeseFactory:
                case Establishment.ConvenienceStore:
                case Establishment.FamilyRestaurant:
                case Establishment.Forest:
                case Establishment.FruitAndVegMarket:
                case Establishment.FurnitureFactory:
                case Establishment.Mine:
                case Establishment.Ranch:
                case Establishment.WheatField:
                    return 6;
                case Establishment.BusinessCenter:
                case Establishment.Stadium:
                case Establishment.TVStation:
                    return ((cardSets & CardSets.BaseGame) != 0) ? 4 : 0;
                case Establishment.GamingMegaStore:
                    return ((cardSets & CardSets.GamingMegaStore) != 0) ? 5 : 0;
                default:
                    throw new NotImplementedException();
            }
        }

        public static int Cost(this Establishment e)
        {
            switch (e)
            {
                case Establishment.AppleOrchard: return 3;
                case Establishment.Bakery: return 1;
                case Establishment.BusinessCenter: return 8;
                case Establishment.Cafe: return 2;
                case Establishment.CheeseFactory: return 5;
                case Establishment.ConvenienceStore: return 2;
                case Establishment.FamilyRestaurant: return 3;
                case Establishment.Forest: return 3;
                case Establishment.FruitAndVegMarket: return 2;
                case Establishment.FurnitureFactory: return 3;
                case Establishment.GamingMegaStore: return 7;
                case Establishment.Mine: return 6;
                case Establishment.Ranch: return 1;
                case Establishment.Stadium: return 6;
                case Establishment.TVStation: return 7;
                case Establishment.WheatField: return 1;
                default:
                    throw new NotImplementedException();
            }
        }

        public static int Cost(this Landmarks l)
        {
            int cost = 0;

            if ((l & Landmarks.AmusementPark) != 0) cost += 16;
            if ((l & Landmarks.RadioTower) != 0) cost += 22;
            if ((l & Landmarks.ShoppingMall) != 0) cost += 10;
            if ((l & Landmarks.TrainStation) != 0) cost += 4;

            return cost;
        }

        public static bool IsMajor(this Establishment e)
        {
            switch (e)
            {
                case Establishment.BusinessCenter:
                case Establishment.GamingMegaStore:
                case Establishment.Stadium:
                case Establishment.TVStation:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsCow(this Establishment e)
        {
            return (e == Establishment.Ranch);
        }

        public static bool IsGear(this Establishment e)
        {
            return (e == Establishment.Mine) || (e == Establishment.Forest);
        }

        public static bool IsWheat(this Establishment e)
        {
            return (e == Establishment.WheatField) ||
                (e == Establishment.AppleOrchard);
        }

        public static bool IsCup(this Establishment e)
        {
            return (e == Establishment.Cafe) || 
                (e == Establishment.FamilyRestaurant);
        }

        public static bool IsShop(this Establishment e)
        {
            return (e == Establishment.Bakery) || 
                (e == Establishment.ConvenienceStore);
        }

        /// <summary>
        ///     Generate a normally-distributed random number.
        /// </summary>
        /// <param name="rng">The RNG to use.</param>
        /// <param name="mu">The mean of the normal distribution.</param>
        /// <param name="sigma">The standard deviation of the normal distribution.</param>
        /// <returns>The random number.</returns>
        internal static double BoxMuller(this Random rng, double mu, double sigma)
        {
            double u = rng.NextDouble(), v = rng.NextDouble();

            // the Box-Muller method generates a pair of independent random numbers X and Y;
            // we only use X here.
            return Math.Sqrt(-2 * Math.Log(u)) * Math.Cos(2 * Math.PI * v);
        }

        internal static void Crossover<T>(T x, T y, Random rng, out T child0, out T child1)
        {
            if (rng.NextDouble() < 0.5)
            {
                child0 = x;
                child1 = y;
            }
            else
            {
                child1 = x;
                child0 = y;
            }
        }
    }
}
