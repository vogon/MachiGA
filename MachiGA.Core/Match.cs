﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachiGA.Core
{
    public struct MatchStatistics
    {
        public int? Winner;
        public double WinnerFitness;

        public int Games;
        public int Turns;
        public int Tkos;
    }

    public class Match
    {
        private int bestOf;
        private Agent2 a;
        private Agent2 b;
        private Func<GameResult, int, double> fitness;

        private MatchStatistics statistics;

        public Match(int bestOf, Agent2 a, Agent2 b, Func<GameResult, int, double> fitness)
        {
            this.bestOf = bestOf;
            this.a = a;
            this.b = b;
            this.fitness = fitness;

            this.statistics = new MatchStatistics();
        }

        public void Play()
        {
            int aGameWins = 0, bGameWins = 0;
            double aFitness = 0.0, bFitness = 0.0;
            int tkos = 0;

            for (int game = 0; game < bestOf; game++)
            {
                // always let the current underdog go first to try and balance series
                // out if the first/second player has an advantage
                bool aIsWhite = (aGameWins < bGameWins);

                Game g = new Game(CardSets.MiniMachi,
                    new Agent2[] {
                                aIsWhite ? a : b,
                                aIsWhite ? b : a
                            });

                GameResult result = g.Play();

                // total up the results
                statistics.Turns += result.NTurns;

                if (aIsWhite && result.Winner == 0 || !aIsWhite && result.Winner == 1)
                {
                    // player A won
                    aGameWins++;
                    aFitness += this.fitness(result, 0);
                    bFitness += this.fitness(result, 1);
                }
                else
                {
                    // player B won
                    bGameWins++;
                    aFitness += this.fitness(result, 0);
                    bFitness += this.fitness(result, 1);
                }

                if (result.Ending == GameEnding.Tko) tkos++;

                // bail out of a match if one player has already won a majority of the games
                double majority = Math.Ceiling((float)bestOf / 2);

                if (aGameWins >= majority)
                {
                    statistics.Winner = 0;
                    statistics.WinnerFitness = aFitness;

                    statistics.Games = game + 1;
                    statistics.Tkos = tkos;
                    return;
                }
                else if (bGameWins >= majority)
                {
                    statistics.Winner = 1;
                    statistics.WinnerFitness = bFitness;

                    statistics.Games = game + 1;
                    statistics.Tkos = tkos;
                    return;
                }
            }
        }

        public MatchStatistics Statistics
        {
            get
            {
                return this.statistics;
            }
        }
    }
}
