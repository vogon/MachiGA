﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MachiGA.Core;

namespace MachiGA
{
    class Program
    {
        //static void RunMatch(int bestOf, Agent2 a, Agent2 b, int aId, int bId, TournamentStatistics stats)
        //{
        //    Match m = new Match(bestOf, a, b, GameFitness);

        //    m.Play();

        //    MatchStatistics matchStats = m.Statistics;

        //    stats.ReportGame(matchStats.Turns);
        //    stats.ReportMatch((matchStats.Winner == 0) ? aId : bId, matchStats.WinnerFitness, matchStats.Games, matchStats.Tkos);
        //}

        static void Main(string[] args)
        {
            const int POPULATION_SIZE = 100;
            const int GAMES_PER_MATCH = 21;
            double mutationRate = 0.01, mutationSeverity = 0.02;
            Random rng = new Random();

            Agent2[] population = new Agent2[POPULATION_SIZE];

            // generate initial population
            for (int i = 0; i < POPULATION_SIZE; i++)
            {
                population[i] = new Agent2(Chromosome2.Random(rng));
            }

            int generation = 0;

            while (true)
            {
                Console.WriteLine("========== generation {0} ==========", generation);

                if (generation % 10 == 0)
                {
                    using (FileStream dump = File.Open(string.Format("D:\\projects\\MachiGA\\data\\gen{0}.bin", generation), FileMode.Create))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.Serialize(dump, population.Select(a => a.Chromosome).ToArray());
                    }
                }

                // host a (round-robin for now) tournament
                ITournament tourney = new RoundRobinTournament(GAMES_PER_MATCH, population);
                DateTime tournamentStart = DateTime.Now;

                tourney.Start();
                tourney.AwaitCompletion();

                DateTime tournamentEnd = DateTime.Now;
                TimeSpan timeElapsed = tournamentEnd - tournamentStart;

                TournamentStatistics stats = tourney.Statistics;

                Console.WriteLine();
                Console.WriteLine("tournament finished in {3} seconds; {2} games, {0} games/sec ({1} matches/sec.)",
                    stats.Games / timeElapsed.TotalSeconds, stats.Matches / timeElapsed.TotalSeconds, stats.Games,
                    timeElapsed.TotalSeconds);
                Console.WriteLine("{0}% of games were TKOs, average game length {1} turns.",
                    (float)stats.Tkos / (float)stats.Games * 100, (float)stats.Turns / (float)stats.Games);

                if (generation % 10 == 0)
                {
                    using (FileStream statsDump = File.Open(string.Format("D:\\projects\\MachiGA\\data\\gen{0}-results.bin", generation), FileMode.Create))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.Serialize(statsDump, stats);
                    }
                }

                // select the top half of players
                Dictionary<IAgent, double> fitness = tourney.Results;
                KeyValuePair<IAgent, double>[] leaders = fitness.OrderByDescending(res => res.Value).Take(3).ToArray();

                Console.WriteLine("leaders: {0} with {1}, {2} with {3}, {4} with {5}.",
                    leaders[0].Key, leaders[0].Value, leaders[1].Key, leaders[1].Value, leaders[2].Key, leaders[2].Value);

                // make new players
                List<Agent2> nextGeneration = new List<Agent2>();

                // roulette-wheel selection:
                // compute normalized fitness for all individuals based on number of wins
                IList<KeyValuePair<IAgent, double>> accumNormFitness = new List<KeyValuePair<IAgent, double>>();
                double totalFitness = leaders.Sum(kv => kv.Value);
                double totalNormFitness = 0;

                foreach (IAgent agent in fitness.Keys)
                {
                    double normFitness = (double)fitness[agent] / (double)totalFitness;
                    totalNormFitness += normFitness;

                    accumNormFitness.Add(new KeyValuePair<IAgent, double>(agent, totalNormFitness));
                }

                // select new individuals
                for (int n = 0; n < POPULATION_SIZE / 2; n++)
                {
                    double motherANF = rng.NextDouble(), fatherANF = rng.NextDouble();
                    Chromosome2? mother = null, father = null;
                    bool motherSelected = false, fatherSelected = false;

                    foreach (var kv in accumNormFitness)
                    {
                        if (!motherSelected && kv.Value > motherANF)
                        {
                            mother = (kv.Key as Agent2).Chromosome;
                            motherSelected = true;
                        }

                        if (!fatherSelected && kv.Value > fatherANF)
                        {
                            father = (kv.Key as Agent2).Chromosome;
                            fatherSelected = true;
                        }

                        if (motherSelected && fatherSelected) break;
                    }

                    Chromosome2 mutatedMother = mother.Value.Mutate(mutationRate, mutationSeverity, rng),
                        mutatedFather = father.Value.Mutate(mutationRate, mutationSeverity, rng);

                    Chromosome2[] children = Chromosome2.Crossover(mutatedMother, mutatedFather, rng);

                    nextGeneration.AddRange(children.Select(child => new Agent2(child)));
                }

                // move on to the next generation
                population = nextGeneration.ToArray();
                generation++;

                if (generation % 100 == 0)
                {
                    mutationSeverity *= 0.95;
                }
            }
        }
    }
}
